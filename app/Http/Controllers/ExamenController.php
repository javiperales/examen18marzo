<?php

namespace App\Http\Controllers;

use App\Exam;
use App\Module;
use App\User;

use Illuminate\Http\Request;

class ExamenController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index()
    {
        $modulos = Module::all();
        $examenes =Exam::paginate(15);
        $usuarios = User::all();

        return view('examenes.index',['examenes'=>$examenes], ['modulos'=>$modulos], ['usuarios'=>$usuarios]);
    }



    public function destroy($id)
    {
        Exam::destroy($id);
        return back();
    }




}//class

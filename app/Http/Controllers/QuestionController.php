<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Module;


class QuestionController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');

    }

    public function index()
    {
        $questions =Question::all();

        return view('questions.index',['questions'=>$questions]);
    }


  public function create()
  {
    $modulos = Module::all();

    return view('questions.create', ['modulos'=>$modulos]);
  }

  public function store(Request $request){

        $reglas = [
            'text' => 'required|max:25',
            'a' => 'required|max:25',
            'b' => 'required|max:25',
            'c' => 'required|max:25',
            'd' => 'required|max:25',
            'answer' => 'required|max:1'
        ];
        $request->validate($reglas);
        $questions = new Question();
        $questions->fill($request->all());
        $questions->save();
        return redirect('/questions');
  }
}

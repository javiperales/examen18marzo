<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{

     protected $fillable =[
        'tittle', 'date', 'materia' , 'acciones'
    ];

         protected $dates =[
       'date'
   ];

   public function questions(){
    return $this->belongsToMany(Question::class);
   }



}

@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">lista de examenes<br>



          <div class="card-body">
           <table class="table">
            <tr>
              <td>titulo</td>
              <td>fecha</td>
              <td>modulo</td>



              <td>id</td>
            </tr>
            @forelse($examenes as $examen)
            <tr>
              <td>{{$examen->title}}</td>
              <td>{{$examen->date->format('d-m-Y')}}</td>

               @foreach ($modulos as $modulo)
              @if($examen->module_id == $modulo->id)
              <td>{{ $modulo->name}}</td>
              @endif
                @endforeach

               {{--  @foreach($usuarios as $usuario)
                @if($examen->user_id == $usuario->id)
                  <td>{{$usuario->name}}</td>
                @endif
                @endforeach --}}


              <td>

                <form method="post" action="/exams/{{ $examen->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                <input type="submit" value="borrar">
            </form>
              </td>
            </tr>
            @empty
            @endforelse

          </table>
 {{ $examenes->render() }}

          <br>
        </div>

      </div>
    </div>
  </div>
</div>
</div>

@endsection

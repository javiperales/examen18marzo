@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Crear nuevo pregunta<br>


                    <div class="card-body">
                        <form method="post" action="/questions">
                           <select name="id">
                            @foreach ($modulos as $modulo)
                            <option value="{{ $modulo->id }}"
                                {{ old('id') == $modulo->id ?
                                'selected="selected"' :
                                ''
                            }}>{{ $modulo->name }}
                        </option>
                        @endforeach
                        <div class="alert alert-danger">
                            {{ $errors->first('id') }}
                        </div>
                    </select>

                    <br>

                    {{ csrf_field() }}
                    <label>enuciado:</label>
                    <input type="text" name="name" value="{{ old('name') }}">


                    <br>

                    <label>opcion a</label>
                    <input type="text" name="price" value=" {{ old('price') }}">
                    <div class="alert alert-danger">
                        {{ $errors->first('price') }}
                    </div>
                    <br>

                    <label>opcion b</label>
                    <input type="text" name="price" value=" {{ old('price') }}">
                    <div class="alert alert-danger">
                        {{ $errors->first('price') }}
                    </div>
                    <br>

                    <label>opcion c</label>
                    <input type="text" name="price" value=" {{ old('price') }}">
                    <div class="alert alert-danger">
                        {{ $errors->first('price') }}
                    </div>
                    <br>

                    <label>opcion d</label>
                    <input type="text" name="price" value=" {{ old('price') }}">
                    <div class="alert alert-danger">
                        {{ $errors->first('price') }}
                    </div>
                    <br>

                    <label>respuesta</label>
                    <input type="text" name="price" value=" {{ old('price') }}">
                    <div class="alert alert-danger">
                        {{ $errors->first('price') }}
                    </div>
                    <br>

                    <br>
                    @if(!count($modulos)==0)
                    <input type="submit" value="Crear">
                    @else
                    <div >
                        <br>

                    </div>
                    @endif

                </form>

            </div>
        </div>
    </div>
</div>
</div>
@endsection

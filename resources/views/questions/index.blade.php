@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">lista de preguntas<br>



          <div class="card-body">

            <a href="/questions/create">Crear preguntas</a>
           <table class="table">
            <tr>
              <td>enuciado</td>
              <td>a</td>
              <td>b</td>
              <td>c</td>
              <td>d</td>
            </tr>
            @forelse($questions as $question)
            <tr>
              <td>{{$question->text}}</td>
              <td>{{$question->a}}</td>
              <td>{{$question->b}}</td>
              <td>{{$question->c}}</td>
              <td>{{$question->d}}</td>
              <td>{{$question->answer}}</td>
              <td>
              </td>
            </tr>
            @empty
            @endforelse

          </table>

          <br>
        </div>

      </div>
    </div>
  </div>
</div>
</div>

@endsection
